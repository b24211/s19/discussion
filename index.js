// If-else statements
let numA = -1;

/*
	Syntax:
		if(condition){
			statement/s;
		}
*/

if(numA < 0){
	console.log("Hola!");
}
console.log(numA > 0);

let city = "New york";
if(city === "New York"){
	console.log("Welcome to New York City!");
}
console.log(city === "New York");

/*
	else if statement
	Syntax:
		if(condition){
			statement/s;
		}
		else if(condition){
			statement/s;
		}
*/

let numH = 1;

if(numA > 0){
	console.log("Hello");
}
else if(numH > 0){
	console.log("World");
}

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}
else{
	console.log("City not included.")
}

// Functions containing if else statements

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return 'Not a Typhoon yet.';
	}
	else if(windspeed <= 61){
		return 'Tropical depression detected';
	}
	else if(windspeed >= 62 && windspeed <=88){
		return 'Tropical storm detected';
	}
	else if(windspeed >=89 || windspeed <= 117){
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected';
	}
}
let message = determineTyphoonIntensity(70);
console.log(message);

// Condition (Ternanty) Operator
/*
	Syntax:
		(condition) ? true : false;
*/

let ternanyResult = (1 < 18) ? true : false;
console.log("Result of Ternany Operator: " + ternanyResult);
let ternanyResult1 = (1 < 18) ? "Value is less than 18" : "Value is greater than 18";
console.log("Result of Ternany Operator: " + ternanyResult1);

// Multiple statement execution
let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in fucntions: " + legalAge + ", " + name);

// Switch statement - Evaluates an expression and matches the expression's value to a case clause.
/*
	Syntax:
		switch(expression){
			case value1:
				statement/s;
				break;
			case value2:
				statement/s;
				break;
			default:
				statement/s;
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tueday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
}

// Try-Catch-Finally Statement

function showIntensityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);

		console.warn(error.message)
	}
	finally{
		alert("Intensity updates will show new alert.")
	}
}

showIntensityAlert(56);